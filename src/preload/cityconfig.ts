import { City } from '../app/models/city';
import { CitiesService } from '../app/services/cities.service';
import { ReflectiveInjector } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { getAnnotations } from '../utils';
import { API_URL } from '../app/app.values';

const injector = ReflectiveInjector.resolveAndCreate(getAnnotations(HttpClientModule)[0].providers);
const http = injector.get(HttpClient);

export class CityConfig {
	private static URL_PATH = API_URL+'cities';
	private static MAPS_API_KEY = 'AIzaSyCLEC3PFp5-TKMFXPiReXqTGuSae8FK0yM';

	private static resolve:any;
	private static reject:any;

	private static findCityById(city_id:Number){
		var index = 0; var pos = -1;

		while(index < CitiesService.cities.length && pos == -1){
			var city = CitiesService.cities[index];

			if(city.id == city_id){
				pos = index;
			}else index++;
		}

		return pos;
	}

	private static findCityByName(city_name:String){
		var index = 0; var pos = -1;

		while(index < CitiesService.cities.length && pos == -1){
			let city = CitiesService.cities[index];

			if(city.name == city_name){
				pos = index;
			}else index++;
		}

		return pos;
	}

	private static jsonToCities(data){
		CitiesService.cities = [];

		for (var i = 0; i < data.length; i++)
			CitiesService.cities.push(new City(data[i].id, data[i].name, data[i].country));
	}

	private static loadCities(){

		if(sessionStorage.getItem('cities') == null)
			http.get(CityConfig.URL_PATH).subscribe((r) => {
				CityConfig.jsonToCities(r.cities);
				sessionStorage.setItem('cities', JSON.stringify(r.cities));

				CityConfig.getCity();
			}, (error) => {
				console.error('Loading error!');
			});
		else{
			CityConfig.jsonToCities(JSON.parse(sessionStorage.getItem('cities')));
			CityConfig.getCity();
		}
	}

	private static getCity(){
		var city_id_Saved:Number = parseInt(localStorage.getItem("city"));

		var pos = -1;
		if(city_id_Saved)
			pos = CityConfig.findCityById(city_id_Saved);

		if(pos > -1){
			CitiesService.city = CitiesService.cities[pos];
			CityConfig.resolve();
		}
		else
			CityConfig.getGeoCity();
	}

	static configure() {
		return new Promise(function (resolve, reject) {
			CityConfig.resolve= resolve;
			CityConfig.reject= reject;

			CityConfig.loadCities();
		});
	}

	private static showCitySelector(){
		(document.querySelector("#citySelector") as HTMLElement).style.display="block";
		(document.querySelector(".spinner") as HTMLElement).style.display="none";
		
		var card_grid = document.querySelector(".card-grid") as HTMLInputElement;
		var cities = CitiesService.cities;

		for (let i = 0; i < cities.length; i++){
			let card_a = document.createElement('a');
			card_a.setAttribute('href', '#');
			card_a.setAttribute('data-id', i+'');

			let card = document.createElement('div');
			card.className = "card";

			let card_thumbnail = document.createElement('div');
			card_thumbnail.className = "card-thumbnail";

			let card_presentation = document.createElement('div');
			card_presentation.className = "card-presentation";

			let h3 = document.createElement('h3');
			h3.className = "title ellipsis";
			h3.innerText = cities[i].name+'';
			card_presentation.appendChild(h3);

			let p = document.createElement('p');
			p.className = "ellipsis";
			p.innerText = cities[i].country+'';
			card_presentation.appendChild(p);

			card_thumbnail.appendChild(card_presentation);
			card.appendChild(card_thumbnail);

			card_a.appendChild(card);
			card_a.addEventListener('click', function(e){
				e.preventDefault();

				let pos = this.getAttribute('data-id');

				CitiesService.city = CitiesService.cities[pos];
				localStorage.setItem("city", CitiesService.cities[pos].id+'');

				CityConfig.resolve();
			}, false);

			card_grid.appendChild(card_a);
		}
	}

	private static getGeoCity() {
		navigator.geolocation.getCurrentPosition((position) =>{
			var GEOCODING = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + '%2C' + position.coords.longitude + '&language=es';

			if(CityConfig.MAPS_API_KEY != '')
				GEOCODING += "&key="+CityConfig.MAPS_API_KEY;
			
			http.get(GEOCODING).subscribe((res) => {
				var l = res;

				if(l.results.length > 0){
					var city = l.results[0].address_components[2].long_name.normalize('NFD').replace(/[\u0300-\u036f]/g, "");

					var pos = CityConfig.findCityByName(city);

					if(pos > -1){
						CitiesService.city = CitiesService.cities[pos];
						localStorage.setItem("city", CitiesService.cities[pos].id+'');
						CityConfig.resolve();

					}else CityConfig.showCitySelector();
				}else CityConfig.showCitySelector();

			}, (error) => {
				console.error(error);

				CityConfig.showCitySelector();
			});

		}, (error) => {
			console.error(error);

			CityConfig.showCitySelector();
		});
	}
}