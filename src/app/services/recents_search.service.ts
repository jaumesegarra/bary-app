import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class RecentsSearchService{
	private _terms:string[] = [];
	public terms:BehaviorSubject<string[]> = new BehaviorSubject<string[]>(this._terms);

	constructor(){
		if(localStorage.getItem('recent_search')){
			this._terms = JSON.parse(localStorage.getItem('recent_search'));
			this.terms.next(this._terms);
		}
	}

	public add(term:string){
		if(this._terms.indexOf(term) == -1){
			this._terms.splice(0,0, term);

			if(this._terms.length > 5)
				this._terms.splice(5,1);

			localStorage.setItem('recent_search', JSON.stringify(this._terms));

			this.terms.next(this._terms);
		}
	}

	public delete(term:string){
		let pos = this._terms.indexOf(term);
		if(pos > -1){
			this._terms.splice(pos,1);
			localStorage.setItem('recent_search', JSON.stringify(this._terms));
			this.terms.next(this._terms);
		}
	}
}