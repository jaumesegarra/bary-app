import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable()
export class UserService{

	constructor(){}

	private _user:User;

	get info():User{
		return this._user;
	}

	set info(user:User){
		this._user = user;
	}

	get token():String{
		return (localStorage.getItem('user.token'));
	}

	public isLogged():boolean {
		return (this.token != null);
	};

	public remove_session(refresh:boolean = true){

		localStorage.removeItem('user.token');
		sessionStorage.removeItem('user.data');
		
		if(refresh)
			location.reload();
	}
}