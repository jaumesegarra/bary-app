import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import { City } from '../models/city';

@Injectable()
export class CitiesService {

	public static cities: Array<City>;
	public static city: City;

	static get(){
		return CitiesService.cities;
	}

	static getCity() {
		return CitiesService.city;
	}
}