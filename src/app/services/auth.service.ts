import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../app.values';

import { UserService } from './user.service';
import { User } from '../models/user';

@Injectable()
export class AuthService{

	constructor(private http:HttpClient, private User:UserService){}

	public profile(): Observable<any>{
		return this.http.get(API_URL+'user/profile');
	}

	public login(user:any): Observable<any>{
		return Observable.create(observer => {
			this.http.post(API_URL+'user/login', user).subscribe((res:any) => { 
				observer.next(res);

				if(res.status==1){
					localStorage.setItem('user.token', res.token);
					window.location.reload();
				}

				observer.complete();
			}, (error) => {
				var code = 0;
				if(error.status == 403)
					code = -1;
				
				observer.next(code);
				observer.complete();
			});
		});
	}

	public signout(){
		this.http.post(API_URL+'user/logout',{}).subscribe((res:any) => { 

			if(res.status==1) this.User.remove_session();
		}, (error) => {
			console.error(error);
		});
	}

	public reset_password(email:string){
		return this.http.post(API_URL+'user/recovery', {'email': email});
	}

	public signup(user:any){
		return Observable.create(observer => {
			this.http.post(API_URL+'user/signup', user).subscribe((res:any) => { 
				observer.next(res);

				if(res.status==1){
					localStorage.setItem('user.token', res.token);
					window.location.reload();
				}

				observer.complete();
			}, (error) => {
				var err = "Server Error";

				if(error.status == 422){
					var errors = error.error.error.errors;
					err = errors[Object.keys(errors)[0]][0]
				}

				observer.next({'error': err});
				observer.complete();

				console.error(error);
			});
		});
	}

	public load_info(){

		var promise = new Promise(function (resolve, reject) {
			if(this.User.isLogged()){
				if(sessionStorage.getItem('user.data') === null){
					promise = this.http.get(API_URL+'user/info', {}).subscribe((res:any) => {

						if(res.token){
							localStorage.setItem('user.token', res.token);
							delete res.token;
						}

						this.User.info = new User(res.name, res.surname, res.confirmed_email);
						this.User.info.save();

						resolve();
					}, (error) => {
						console.log('error');
						resolve();
					});
				}else{ 
					let user = JSON.parse(sessionStorage.getItem('user.data'));
					this.User.info = new User(user._name, user._surname, user._confirmed_email);
					
					resolve();
				};
			}else resolve();

		}.bind(this));

		return promise;
	}


	public requiredAdditionalFields():Observable<any[]>{
		return this.http.get<any>(API_URL+'user/required_additional_fields', {}).map(res => {
			return res;
		});
	}

	public saveAdditionalFields(data):Observable<any>{
		return this.http.post<any>(API_URL+'user/required_additional_fields', data).map(res => {
			return res;
		});
	}

	public resendEmailConfirmation():Observable<any>{
		return this.http.post<any>(API_URL+'user/resend', {});
	}
}