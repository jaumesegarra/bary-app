import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse,
  HttpInterceptor
} from '@angular/common/http';
import 'rxjs/add/operator/do';

import { UserService } from '../services/index';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class InvalidTokenInterceptor implements HttpInterceptor {

  constructor(public user: UserService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {

      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        
        if (err.status === 500 && err.error && err.error.error && err.error.error.message == "Unauthenticated.")
          this.user.remove_session((!err.url.includes('user/info')));
      }
    });
  }
}
