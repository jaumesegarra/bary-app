import { Component, OnInit } from '@angular/core';

import { City } from '../../models/city';
import { CitiesService } from '../../services/index';

@Component({
	selector: '.city-selector',
	template: `
	<dropdown [_label]="'%name%, %country%'" [options]="cities" [(value)]="city" (onChange)="changeCity()" [searchAttributes]="'name,country'"></dropdown>
	`,
	providers: [CitiesService]
})

export class CitySelectorComponent {
	city: City;
	cities:Array<City>;

	constructor (){
		this.cities = CitiesService.get();
		this.city = CitiesService.getCity();
	}

	private saveCity(_city){
		localStorage.setItem("city", _city.id);
		location.reload();
	}

	changeCity(){
		this.saveCity(this.city);
	}
}
