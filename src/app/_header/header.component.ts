import { Component, ElementRef, Input, ViewChild, HostBinding } from '@angular/core';

import { AuthService, UserService } from '../services/index';

import {Router} from '@angular/router';

@Component({
	selector: 'header',
	templateUrl: './header.component.pug'
})

export class HeaderComponent {
	@Input() parent;

	private isLogged: boolean;

	private name:string;

	private _menu_option = "";
	private USER_MENU = [
	{
		title: 'Perfil',
		run: function () {
			this.route.navigate(['/me']);
		}.bind(this)
	},
	{
		title: 'Configuración',
		run: function () {
			this.route.navigate(['/settings']);
		}.bind(this)
	},
	{
		title: 'Cerrar sesión',
		run: function () {
			this.Auth.signout();
		}.bind(this)
	}
	];
	
	constructor(
		private User: UserService,
		private Auth: AuthService,
		private route:Router
		) {

		if(this.User.info)
			this.name = this.User.info.name;
	}

	private login(){
		this.parent.loginModal.show();
	}
}