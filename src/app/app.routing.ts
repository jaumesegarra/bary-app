import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorComponent } from './pages/error/error.component';

const appRoutes: Routes = [
{
	path: '',
	redirectTo: 'discover',
	pathMatch: "full"
},
{ path: '404', component: ErrorComponent},
{ path: '**', redirectTo: '404'}
];

export const routingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: false });
