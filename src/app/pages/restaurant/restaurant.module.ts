import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../_shared_module/shared.module';

import { RestaurantComponent } from './restaurant.component';
import { RestaurantRoutingModule } from './restaurant-routing.module';
import { RestaurantResolver } from './restaurant.resolver';

import { RestaurantReserveComponent } from './_reserve/reserve.component';
import { RestaurantPhotoSliderComponent } from './_photo-slider/photo-slider.component';
import { RestaurantReviewsComponent } from './_reviews/reviews.component';
import { RestaurantReviewsStarsComponent } from './_reviews/_stars-ipt/stars-ipt.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE} from 'ng-pick-datetime';

@NgModule({
	declarations: [
	RestaurantComponent,
	RestaurantReserveComponent,
	RestaurantPhotoSliderComponent,
	RestaurantReviewsComponent,
	RestaurantReviewsStarsComponent
	],
	imports: [
	SharedModule,
	BrowserModule,
	FormsModule,
	RestaurantRoutingModule,
	BrowserAnimationsModule,
	OwlDateTimeModule, OwlNativeDateTimeModule
	],
	providers:[RestaurantResolver],
	bootstrap: [RestaurantComponent]
})
export class RestaurantModule { }
