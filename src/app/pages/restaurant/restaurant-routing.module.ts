import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RestaurantComponent } from './restaurant.component';
import { RestaurantResolver } from './restaurant.resolver';

const restaurantRoutes: Routes = [
{
	path: 'restaurant/:id',
	component: RestaurantComponent,
	resolve: { restaurant: RestaurantResolver },
	children: []
}
];

@NgModule({
	imports: [
	RouterModule.forChild(restaurantRoutes)
	],
	exports: [
	RouterModule
	]
})

export class RestaurantRoutingModule { }