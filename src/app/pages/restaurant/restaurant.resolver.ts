import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { RestaurantService, LoaderService } from '../../services/index';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class RestaurantResolver implements Resolve<Observable<any>> {	
	constructor(private restaurantService: RestaurantService, private loaderService: LoaderService, private router:Router) {}

	resolve(route: ActivatedRouteSnapshot) {
		this.loaderService.display(true);

		return this.restaurantService.get(+route.paramMap.get('id')).do(data => {
			this.loaderService.display(false);
		}).catch((e) => {
			this.loaderService.display(false);
			
			if(e.status == 404)
				this.router.navigate(['./404']);

			return Observable.of({});
		});
	}
}