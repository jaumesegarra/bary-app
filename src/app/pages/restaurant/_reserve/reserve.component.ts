import { Component, Input, OnInit } from '@angular/core';
import { UserService, AuthService, RestaurantService } from '../../../services/index';
import * as moment from 'moment';

@Component({
	selector: '.reserve',
	templateUrl: './reserve.component.pug',
	styleUrls: ['./reserve.component.scss']
})

export class RestaurantReserveComponent{
	@Input()
	private restaurant_id;

	@Input()
	private available_days;

	private template_num:number = 1;
	private date_hours:string[];
	private add_fields:string[];

	private date:Date;
	private hour:any;
	private num_pers:number = 0;
	private suggestions:string;

	private error:string;

	constructor(
		private user:UserService, 
		private authService:AuthService, 
		private restaurantService:RestaurantService
		){}

	ngOnInit() {
		if(this.available_days === null)
			this.template_num = -2;
		else if(!this.user.isLogged())
			this.template_num = -1;
	}

	public date_filter = (d: Date): boolean => {
		let enabled = false;

		if(this.available_days){
			let date_str = moment(d).format('YYYY-MM-DD');
			enabled = (this.available_days.indexOf(date_str) >= 0);
		}

		return enabled;
	}

	private goTemplate(num){
		this.template_num = num;
	}

	private reset(){
		this.date = undefined;
		this.hour = undefined;
		this.num_pers = 0;
		this.suggestions = undefined;
	}

	private goDate(){
		this.reset();
		this.goTemplate(1);
	}

	private goHours(event){
		if(event){
			this.goTemplate(0);
			this.restaurantService.getAvailableReserveHours(this.restaurant_id, moment(this.date).format('YYYY-MM-DD')).subscribe(res => {
				this.date_hours = res.hours;
				this.goTemplate(2);
			}, error => {
				console.error(error);
				this.goTemplate(1);
			})
		}
	}

	private goFinish(){
		this.goTemplate(0);
		this.authService.requiredAdditionalFields().subscribe(res => {
			this.add_fields = res;
			this.goTemplate(3);
		}, error => {
			console.error(error);
			this.goTemplate(2);
		});
	}

	private save_reserve(){
		this.restaurantService.saveReserve(this.restaurant_id, moment(this.date).format('YYYY-MM-DD'), this.hour.hour, this.num_pers, this.suggestions).subscribe(res => {
			this.goTemplate(4);
			setTimeout(function () {
				this.goDate();
			}.bind(this), 2000);
		}, error => {
			console.error(error);
			this.goTemplate(3);
		});
	}

	private finish(data){
		if(data.status == "VALID")
		{	
			let attrs = data.form.value;
			this.suggestions = attrs.suggestions;

			this.goTemplate(0);

			if(this.add_fields.length == 0)
				this.save_reserve();
			else{
				let body = attrs;
				delete body.suggestions;

				this.authService.saveAdditionalFields(body).subscribe(data => {
					if(data.status == 1)
						this.save_reserve();
					else
						this.goTemplate(3);
				}, error => {
					let errors = error.error.error.errors;

					console.error(errors);
					
					this.error = errors[Object.keys(errors)[0]][0] || 'Server error.';
					this.goTemplate(3);
				});
			}
		}
	}
}