import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: '.stars-ipt',
	templateUrl: './stars-ipt.component.pug',
	styleUrls: ['./stars-ipt.component.scss']
})

export class RestaurantReviewsStarsComponent{
	@Input()
	rate:number;
	@Output()
	onChange:EventEmitter<number> = new EventEmitter<number>();

	constructor(){}

	update(n:number){
		this.rate = n;
		this.onChange.emit(n);
	}
}