import { Component, Input, OnInit } from '@angular/core';
import { LoaderService, RestaurantService } from '../../../services/index';
import { NotificationsService } from 'angular2-notifications';

@Component({
	selector: '.reviews',
	templateUrl: './reviews.component.pug',
	styleUrls: ['./reviews.component.scss']
})

export class RestaurantReviewsComponent implements OnInit{
	@Input()
	restaurant_id:number;
	@Input()
	reviews:any;
	@Input()
	rate:number;
	@Input()
	allow_review:boolean;
	
	private template_num:number;
	private tmp_review:any;
	private saving_review:boolean;

	constructor(private loaderService: LoaderService, private restaurantService: RestaurantService, private notificationsService: NotificationsService){}

	ngOnInit(){
		this.goSummary();
	}

	goSummary(){
		this.template_num = 1;
	}

	goEdit(){
		this.template_num = 2;
	}

	private clone_review(){
		this.tmp_review = {};

		this.tmp_review.rate = (this.reviews.me) ? this.reviews.me.rate : 0;

		this.tmp_review.title = (this.reviews.me && this.reviews.me.title != null) ? this.reviews.me.title : '';
		this.tmp_review.description = (this.reviews.me && this.reviews.me.description != null) ? this.reviews.me.description : '';
	}

	rate_review(n:number){
		this.clone_review();
		this.tmp_review.rate = n;

		setTimeout(function () {
			this.goEdit();
		}.bind(this),150);
	}

	edit_review(){
		this.clone_review();
		this.goEdit();
	}

	change_tmp_rate_review(n:number){
		this.tmp_review.rate = n;
	}

	save_review(){
		if((this.tmp_review.title.trim() != '' && this.tmp_review.description.trim() != '') || (this.tmp_review.title.trim() == '' && this.tmp_review.description.trim() == '')){
			this.saving_review=true;
			this.loaderService.display(true);

			if(this.tmp_review.title.trim() == ''){
				this.tmp_review.title = undefined;
				this.tmp_review.description = undefined;
			}

			this.restaurantService.writeReview(this.restaurant_id, this.tmp_review.rate, this.tmp_review.title, this.tmp_review.description).subscribe(data => {
				this.saving_review=false;
				this.loaderService.display(false);
				this.notificationsService.success("Genial!", "Reseña enviada con exito.", false);
				this.goSummary();
			}, error => {
				console.error(error);
				this.saving_review=false;
				this.loaderService.display(false);

				this.notificationsService.error("Error", "Error al intentar envíar la reseña.", false);
			});
		}
	}
}