import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { SettingsService } from '../settings.service';
import { LoaderService } from '../../../services/index';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class ProfileResolver implements Resolve<Observable<any>> {	
	constructor(private settingsService: SettingsService, private loaderService: LoaderService, private router:Router) {}

	resolve(route: ActivatedRouteSnapshot) {
		this.loaderService.display(true);

		return this.settingsService.get().do(data => {
			this.loaderService.display(false);
		}).catch((e) => {
			this.loaderService.display(false);

			return Observable.of({});
		});
	}
}