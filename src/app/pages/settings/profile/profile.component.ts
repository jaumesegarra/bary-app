import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LoaderService } from '../../../services/index';
import { NotificationsService } from 'angular2-notifications';

import { SettingsService } from '../settings.service';

@Component({
	selector: 'profile-component',
	templateUrl: './profile.component.pug',
	styleUrls: ['../settings.scss', './profile.component.scss']
})

export class SettingsProfileComponent implements OnInit{
	private data: any;
	private serverErrors: any[] = [];

	constructor (
		private router: Router,
		private activedRoute: ActivatedRoute,
		private notificationsService: NotificationsService,
		private loaderService: LoaderService,
		private settingsService: SettingsService
		){}

	ngOnInit(){
		this.data = this.activedRoute.snapshot.data.profile;
	}

	save(form: any): void{
		this.serverErrors = [];

		let dataForm: any = form.value;
		delete dataForm.email;
		
		if(dataForm.dni === null)
			delete dataForm.dni;

		if(dataForm.phone === null)
			delete dataForm.phone;

		this.loaderService.display(true);
		this.settingsService.save(dataForm).subscribe(res => {
			this.loaderService.display(false);

			let user: any = JSON.parse(sessionStorage.getItem('user.data'));
			user._name = dataForm.name;
			user._surname = dataForm.surname;
			sessionStorage.setItem('user.data', JSON.stringify(user));

			this.notificationsService.success("Genial!", "Datos modificados correctamente.", false);
		}, err => {
			if(err.error && err.error.error && err.error.error.errors)
				this.serverErrors = err.error.error.errors;

			this.loaderService.display(false);
			this.notificationsService.error("Ups..", "Error al modificar los datos.", false);
		})
	}

	goToAccountSettings(): void{
		console.log('eee');
		this.router.navigate(['./settings/account']);
	}
}
