import { Component, ViewChild } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { LoaderService, UserService } from '../../../services/index';

import { SettingsService } from '../settings.service';

@Component({
	selector: 'account-component',
	templateUrl: './account.component.pug',
	styleUrls: ['../settings.scss', './account.component.scss']
})

export class SettingsAccountComponent {

	constructor (
		private settingsService: SettingsService,
		private loaderService: LoaderService,
		private userService: UserService,
		private notificationsService: NotificationsService,
		){}

	@ViewChild('typePasswordModal') typePasswordModal;

	change_email(form: any) : void{
		let dataForm: any = form.value;

		function execute(password) {
			dataForm.password = password;

			this.loaderService.display(true);
			this.settingsService.change_email(dataForm).subscribe(res => {
				this.loaderService.display(false);
				this.notificationsService.success("Genial!", "Email modificado correctamente.", false);
				form.reset();
			}, err => {
				console.error(err);

				let errorMsg: string = "Error al cambiar email.";

				if(err.error && err.error.error && err.error.error.errors)
					if(err.error.error.errors.password)
						errorMsg = 'Contraseña incorrecta!';
					else if(err.error.error.errors.email)
						errorMsg = err.error.error.errors.email;

					this.loaderService.display(false);
					this.notificationsService.error("Ups..", errorMsg, false);
				});
		}

		this.typePasswordModal.open(execute.bind(this));
	}

	change_password(form: any) : void{
		let dataForm: any = form.value;

		function execute(password) {
			dataForm.old_password = password;

			this.loaderService.display(true);
			this.settingsService.change_password(dataForm).subscribe(res => {
				this.loaderService.display(false);
				this.notificationsService.success("Genial!", "Contraseña modificada correctamente.", false);
				form.reset();
			}, err => {
				console.error(err);

				let errorMsg: string = "Error al cambiar contraseña.";

				if(err.error && err.error.error && err.error.error.errors && err.error.error.errors.password)
					errorMsg = 'Contraseña incorrecta!';

				this.loaderService.display(false);
				this.notificationsService.error("Ups..", errorMsg, false);
			});
		}

		this.typePasswordModal.open(execute.bind(this));
	}

	delete_account(): void{
		function execute(password) {

			this.loaderService.display(true);
			this.settingsService.delete_account({
				'password': password
			}).subscribe(res => {
				this.loaderService.display(false);
				this.userService.remove_session();
			}, err => {
				console.error(err);

				let errorMsg: string = "Error al intentar eliminar la cuenta.";

				if(err.error && err.error.error && err.error.error.errors && err.error.error.errors.password)
					errorMsg = 'Contraseña incorrecta!';

				this.loaderService.display(false);
				this.notificationsService.error("Ups..", errorMsg, false);
			});
		}

		this.typePasswordModal.open(execute.bind(this));
	}
}
