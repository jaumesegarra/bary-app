import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { LoaderService, RestaurantService } from '../../../services/index';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class FavouritesResolver implements Resolve<Observable<any>> {	
	constructor(
		private restaurantService: RestaurantService, 
		private loaderService: LoaderService
		) {}

	resolve(route: ActivatedRouteSnapshot) {

		this.loaderService.display(true);

		return this.restaurantService.getFavourites().do(data => {
			this.loaderService.display(false);
		}).catch((e) => {
			this.loaderService.display(false);

			return Observable.of({});
		});
	}
}