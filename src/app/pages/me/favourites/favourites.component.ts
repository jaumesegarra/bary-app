import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoaderService, RestaurantService } from '../../../services/index';

@Component({
	selector: 'favourites-component',
	templateUrl: './favourites.component.pug',
	styleUrls: ['./favourites.component.scss']
})

export class MeFavouritesComponent implements OnInit{
	private page:number = 1;
	private favourites:any;

	constructor (
		private activatedRoute:ActivatedRoute,
		private loaderService: LoaderService,
		private restaurantService: RestaurantService, 
		){}

	ngOnInit() {
		this.favourites = this.activatedRoute.snapshot.data.favourites;
	}

	private unfav(e: MouseEvent, restaurant:any){
		e.stopPropagation();

		this.loaderService.display(true);
		this.restaurantService.addToFavourites(restaurant.id, false).subscribe(
			res => {
				let pos = this.favourites.data.indexOf(restaurant);
				if (pos !== -1)
					this.favourites.data.splice(pos, 1);
				
				this.loaderService.display(false);
			}, err =>{
				console.error(err);
				this.loaderService.display(false);
			}
			);

		return false;
	}

	onResultsScroll(){
		if(this.favourites && this.favourites.pages > this.page){
			this.page++;
			this.loaderService.display(true);

			this.restaurantService.getFavourites(this.page).subscribe(res => {
				for (let restaurant_pos in res.others.data) {
					this.favourites.data.push(res.data[restaurant_pos]);
				}
				this.loaderService.display(false);
			}, error => {
				this.page--;
			});
		}
	}
}
