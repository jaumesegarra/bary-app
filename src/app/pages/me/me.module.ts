import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../_shared_module/shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { MeComponent } from './me.component';
import { MeProfileComponent } from './profile/profile.component';
import { MeReservesComponent } from './reserves/reserves.component';
import { EditReserveComponent } from './reserves/edit_reserve/edit_reserve.component';
import { MeFavouritesComponent } from './favourites/favourites.component';

import { MeRoutingModule } from './me-routing.module';
import { ProfileResolver } from './profile/profile.resolver';
import { ReservesResolver } from './reserves/reserves.resolver';
import { FavouritesResolver } from './favourites/favourites.resolver';

import { ReservesService } from './reserves/reserves.service';

@NgModule({
	declarations: [
	MeComponent,
	MeProfileComponent,
	MeReservesComponent,
	EditReserveComponent,
	MeFavouritesComponent,
	],
	imports: [
	BrowserModule,
	FormsModule,
	MeRoutingModule,
	SharedModule,
	InfiniteScrollModule
	],
	providers: [ProfileResolver, ReservesService, ReservesResolver, FavouritesResolver],
	bootstrap: [MeComponent]
})
export class MeModule { }