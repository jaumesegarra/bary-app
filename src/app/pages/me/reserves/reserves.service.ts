import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

import { API_URL } from '../../../app.values';

@Injectable()
export class ReservesService{

	constructor(private http:HttpClient){}

	public get(p?:number):Observable<any>{
		let page:number = 1;
		if(p)
			page = p;

		let params_obj:any = {
			'p': page
		};

		let params = new HttpParams({
			fromObject: params_obj
		});

		return this.http.get(API_URL+'user/reserves', {params: params});
	}

	public edit(code:string, num_pers_add:number):Observable<any>{
		return this.http.post(API_URL+'user/reserves/'+code+'/edit', {
			'num_pers_additional': num_pers_add
		});
	};

	public cancel(code:string):Observable<any>{
		return this.http.post(API_URL+'user/reserves/'+code+'/cancel', {});
	}
}