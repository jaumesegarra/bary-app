import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { LoaderService, UserService } from '../../../services/index';

import { ReservesService } from './reserves.service';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class ReservesResolver implements Resolve<Observable<any>> {	
	constructor(
		private reservesService: ReservesService, 
		private userService: UserService, 
		private loaderService: LoaderService
		) {}

	resolve(route: ActivatedRouteSnapshot) {
		if(this.userService.isLogged()){

			this.loaderService.display(true);

			return this.reservesService.get().do(data => {
				this.loaderService.display(false);
			}).catch((e) => {
				this.loaderService.display(false);

				return Observable.of({});
			});
		}else return Observable.of(null);
	}
}