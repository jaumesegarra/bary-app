import { ModalComponent } from '../../../../modals/modal.component';
import { Component, Input } from '@angular/core';

import { AuthService, LoaderService } from '../../../../services/index';
import { NotificationsService } from 'angular2-notifications';
import { ReservesService } from '../reserves.service';

@Component({
	selector: 'edit-reserve-modal',
	templateUrl: './edit_reserve.component.pug',
	styleUrls: ['./edit_reserve.component.scss']
})
export class EditReserveComponent extends ModalComponent{
	@Input()
	reserve:any;

	private add_num_pers:number = 0;

	constructor(
		private loaderService:LoaderService, 
		private notificationsService: NotificationsService,
		private reservesService:ReservesService
		){super()}

	public hide(effect:boolean = true): void {
		super.hide(effect);
		this.add_num_pers = 0;
	}

	private edit(): void{
		this.loaderService.display(true);
		this.reservesService.edit(this.reserve.code, this.add_num_pers).subscribe(res => {
			this.loaderService.display(false);

			if(res.status == 1){
				this.reserve.num_pers += this.add_num_pers; 
			}else this.showError();

			this.add_num_pers = 0;
		}, err =>{
			console.error(err);
			this.loaderService.display(false);
			this.showError();
			this.add_num_pers = 0;
		});
		super.hide();
	}

	private showError(): void{
		this.notificationsService.error("Error", "No se ha podido modificar su reserva.", false);
	}
}