import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchComponent } from './search.component';
import { SearchResolver } from './search.resolver';

const searchRoutes: Routes = [
{
	path: 'search',
	component: SearchComponent,
	resolve: { search_results:  SearchResolver }
}
];

@NgModule({
	imports: [
	RouterModule.forChild(searchRoutes)
	],
	exports: [
	RouterModule
	]
})

export class SearchRoutingModule { }