import { BrowserModule } from '@angular/platform-browser';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { SharedModule } from '../../_shared_module/shared.module';

import { SearchComponent } from './search.component';
import { FiltersComponent } from './_filters/filters.component';
import { ResultsComponent } from './_results/results.component';

import { SearchService } from './search.service';
import { SearchResolver } from './search.resolver';

import { SearchRoutingModule } from './search-routing.module';

@NgModule({
	declarations: [
	SearchComponent,
	FiltersComponent,
	ResultsComponent
	],
	imports: [
	BrowserModule,
	InfiniteScrollModule,
	FormsModule,
	SharedModule,
	SearchRoutingModule,
	BrowserAnimationsModule,
	OwlDateTimeModule, OwlNativeDateTimeModule
	],
	providers: [SearchService, SearchResolver],
	bootstrap: [SearchComponent]
})
export class SearchModule { }
