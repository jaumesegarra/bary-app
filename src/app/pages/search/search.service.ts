import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CitiesService } from '../../services/index';

import { API_URL } from '../../app.values';

@Injectable()
export class SearchService{

	constructor(private http:HttpClient){}

	public get(p:number,q?:string, reserve?:any):Observable<any>{
		let params_obj:any = {};
		if(q)
			params_obj.q = q;

		if(reserve && reserve.date && reserve.num_pers){
			params_obj.date = reserve.date; params_obj.num_pers = reserve.num_pers;
		}

		params_obj.p = p;

		let params = new HttpParams({
			fromObject: params_obj
		});
		
		return this.http.get(API_URL+'c/'+CitiesService.getCity().id+'/search', {params:params});
	}
}