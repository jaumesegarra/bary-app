import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { SearchService } from './search.service';
import { LoaderService } from '../../services/index';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class SearchResolver implements Resolve<Observable<any>> {	
	constructor(private searchService: SearchService, private loaderService: LoaderService, private router:Router) {}

	resolve(route: ActivatedRouteSnapshot) {
		this.loaderService.display(true);

		let q:string = (route.params.q || undefined);

		let reserve:any = {};
		if(route.params.date && route.params.pers){
			reserve.date = (route.params.date || undefined);
			reserve.num_pers = (+route.params.pers || undefined);
		}

		let p:number = 1;//(+route.params.p || 1);
		return this.searchService.get(p, q, reserve).do(data => {
			this.loaderService.display(false);
		}).catch((e) => {
			this.loaderService.display(false);

			return Observable.of({});
		});
	}
}