import { Injectable } from '@angular/core';

import { Resolve } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { DiscoverService } from '../discover.service';
import { LoaderService } from '../../../services/index';

import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable()
export class DiscoverAllResolver implements Resolve<Observable<any>> {	
	constructor(private discoverService: DiscoverService, private loaderService: LoaderService, private router:Router) {}

	resolve(route: ActivatedRouteSnapshot) {
		this.loaderService.display(true);

		return this.discoverService.get().do(data => {
			this.loaderService.display(false);
		}).catch((e) => {
			this.loaderService.display(false);

			return Observable.of({});
		});
	}
}