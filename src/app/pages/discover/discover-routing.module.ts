import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DiscoverComponent } from './discover.component';
import { DiscoverAllComponent } from './all/all.component';
import { DiscoverAllResolver } from './all/all.resolver';

const discoverRoutes: Routes = [
{
  path: 'discover',
  component: DiscoverComponent,
  children: [
  {
    path: '',
    component: DiscoverAllComponent,
    resolve: { restaurants: DiscoverAllResolver },
  }
  ]
}
];

@NgModule({
  imports: [
  RouterModule.forChild(discoverRoutes)
  ],
  exports: [
  RouterModule
  ]
})

export class DiscoverRoutingModule { }