import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { SharedModule } from '../../_shared_module/shared.module';

import { DiscoverComponent } from './discover.component';
import { DiscoverRoutingModule } from './discover-routing.module';
import { DiscoverAllComponent } from './all/all.component';

import { DiscoverService } from './discover.service';

import { DiscoverAllResolver } from './all/all.resolver';

@NgModule({
	declarations: [
	DiscoverComponent,
	DiscoverAllComponent
	],
	imports: [
	SharedModule,
	BrowserModule,
	FormsModule,
	DiscoverRoutingModule
	],
	providers: [DiscoverService, DiscoverAllResolver],
	bootstrap: [DiscoverComponent]
})
export class DiscoverModule { }
