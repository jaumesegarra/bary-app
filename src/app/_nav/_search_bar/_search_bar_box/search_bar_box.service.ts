import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { API_URL } from '../../../app.values';

import { HttpClient } from '@angular/common/http';

import { CitiesService } from '../../../services/index';

@Injectable()
export class SearchBarBoxService{

	constructor(private http:HttpClient){}

	public getOutstanding(): Observable<any>{
		return this.http.get(API_URL+'c/'+CitiesService.getCity().id+'/search/outstading');
	}

	public getTerms(query:string): Observable<any>{
		return this.http.get(API_URL+'c/'+CitiesService.getCity().id+'/search/term/'+query);
	}
}