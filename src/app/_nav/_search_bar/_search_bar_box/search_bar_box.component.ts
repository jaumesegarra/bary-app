import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SearchBarBoxService } from './search_bar_box.service';
import { RecentsSearchService } from '../../../services/recents_search.service';

@Component({
	selector: '.results',
	templateUrl: './search_bar_box.component.pug',
	styleUrls: ['./search_bar_box.component.scss'],
	providers: [SearchBarBoxService]
})

export class SearchBarBoxComponent {
	@Input()
	text:string;
	
	@Output()
	textEmitter:EventEmitter<string> = new EventEmitter<string>();

	@Output()
	isLoaded:EventEmitter<boolean> = new EventEmitter<boolean>(true);

	private outstanding:string[];
	private recents:string[];
	private results:object;

	public loaded:boolean = true;

	constructor(
		private search_service:SearchBarBoxService,
		private recent_service:RecentsSearchService
		){}

	ngOnInit(){
		this.search_service.getOutstanding().subscribe(data => this.outstanding = data);
		this.recent_service.terms.subscribe(data => this.recents = data);
	}

	private doSearch(){
		if(this.text && this.text.trim() != "")
		{
			this.loaded=false;
			this.isLoaded.emit(false);

			this.search_service.getTerms(this.text).subscribe(res => {
				this.loaded=true;
				this.isLoaded.emit(true);

				this.results = res;
			});
		}else
		{
			this.loaded=true;
			this.isLoaded.emit(true);
			this.results = undefined;
		}
	}

	ngOnChanges(val){
		this.doSearch();
	}

	private searchText(text){		
		this.text=text;
		this.textEmitter.emit(text);
	}
}