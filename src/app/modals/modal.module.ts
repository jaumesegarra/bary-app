import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { ModalComponent } from './modal.component';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
	declarations: [
	ModalComponent,
	ConfirmComponent
	],
	imports: [
	BrowserModule,
	FormsModule,
	],
	providers: [],
	exports: [ConfirmComponent],
	bootstrap: [ModalComponent]
})
export class ModalModule { }