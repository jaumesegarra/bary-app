import { Component, Input } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-modal',
  template: ''
})

export class ModalComponent{
  @Input() parent;

  public visible = false;
  private visibleAnimate = false;

  public show(effect:boolean = true): void {
    this.visible = true;
    if(effect)
      setTimeout(() => this.visibleAnimate = true, 100);
    else
      this.visibleAnimate = true
  }

  public hide(effect:boolean = true): void {
    this.visibleAnimate = false;
    if(effect)
      setTimeout(() => this.visible = false, 300);
    else
      this.visible = false;
  }

  private onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.hide();
    }
  }

  public goToModal(modal_name: String){
    this.hide(false);
    this.parent[modal_name+''].show(false);
  }
}