import { ModalComponent } from '../modal.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';

import { AuthService } from '../../services/index';

@Component({
	selector: 'confirm-modal',
	templateUrl: './confirm.component.pug',
	styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent extends ModalComponent{
	@Input()
	title:string;

	@Output()
	onClose:EventEmitter<boolean> = new EventEmitter<boolean>();

	constructor(){super()}

	public hide(effect:boolean = true): void {
		super.hide(effect);

		this.onClose.emit(false);
	}

	private closeAndSend(res:boolean){
		this.hide();
		this.onClose.emit(res);
	}
}