import { ModalComponent } from '../modal.component';
import { Component } from '@angular/core';

import { AuthService } from '../../services/index';

@Component({
	selector: 'recover_password-modal',
	templateUrl: './recover_password.component.pug'
})

export class RecoverPasswordComponent extends ModalComponent {
	private loading:boolean = false;
	private error:boolean = true;
	private message:string = null;

	constructor(private auth:AuthService) {super()}

	public recover(data){

		if(!this.loading) 
			if(data.status == "VALID"){
				this.message = null;
				this.error = true;
				this.loading = true;

				var form = data.form.value;

				this.auth.reset_password(form.email).subscribe(res => {

					data.form.reset();
					this.loading = false;
					this.error = false;
					this.message = 'Correo de recuperación enviado correctamente!';
					
				}, (error) => {
					this.loading = false;
					this.error = true;

					if(error.status==422){
						this.message = 'No hay ningún usuario con este correo electrónico';
					}else
					this.message = 'Server error';
				});

			}else{ 
				this.error = true;
				this.message = 'Email no valido o no introducido.';
			}
		}
	}