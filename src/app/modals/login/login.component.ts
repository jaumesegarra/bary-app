import { ModalComponent } from '../modal.component';
import { Component } from '@angular/core';

import { AuthService } from '../../services/index';

@Component({
	selector: 'login-modal',
	templateUrl: './login.component.pug'
})

export class LoginComponent extends ModalComponent{
	private loading:boolean = false;
	private error:string = null;

	constructor(private auth:AuthService){super()}

	public login(data): void {		
		if(!this.loading) 
			if(data.status == "VALID"){
				this.error = null;
				this.loading = true;

				var user = data.form.value;

				this.auth.login(user).subscribe(res => {
					if(res.status < 1 || res <= 0){
						this.loading = false;
						
						this.error = (res == -1) ? 'Credenciales incorrectos' : 'Server error';
					}
				});

			}else this.error = 'Email y/o contraseña no introducidos!';
		}

	}