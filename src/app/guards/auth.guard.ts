import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { UserService } from '../services/index';

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private user: UserService, private router: Router) {}

	canActivate() {
		if(!this.user.isLogged()){
			this.router.navigate(['./']);
			return false;
		}

		return true;
	}
}